//NODE MCU PIN mapping
#define D0                16
#define D1                5
#define D2                4
#define D3                0
#define D4                2
#define D5                14
#define D6                12
#define D7                13
#define D8                15
#define D9                3
#define D10               1

#define MAX_PWM           255 //1023

#define BRAKE 0
#define CW    1
#define CCW   2
#define CS_THRESHOLD 15   // Definition of safety current (Check: "1.3 Monster Shield Example").

#define PWM_MOTOR_1 D6
#define PWM_MOTOR_2 D7
#define PWM_MOTOR_3 D8

//Pin connected to DS of 74HC595
#define dataPin D1
//Pin connected to ST_CP of 74HC595
#define latchPin D2
//Pin connected to SH_CP of 74HC595
#define clockPin D3

//#define CURRENT_SEN_1 2
//#define CURRENT_SEN_2 3

//#define EN_PIN_1 2//A0
//#define EN_PIN_2 3//A1

#define MOTOR_1 0
#define MOTOR_2 1
#define MOTOR_3 2

int M1CW = 1;
int M1CCW = 2;
int M2CW = 3;
int M2CCW = 4;
int M3CW = 5;
int M3CCW = 6;

byte num = 0;
 
void setup()                         
{  
  pinMode(PWM_MOTOR_1, OUTPUT);
  pinMode(PWM_MOTOR_2, OUTPUT);
  pinMode(PWM_MOTOR_3, OUTPUT);

  //set pins to output so you can control the shift register
  pinMode(latchPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);

  analogWriteFreq(5000);
  analogWriteRange(255);

//  pinMode(CURRENT_SEN_1, OUTPUT);
//  pinMode(CURRENT_SEN_2, OUTPUT);  

//  pinMode(EN_PIN_1, OUTPUT);
//  pinMode(EN_PIN_2, OUTPUT);

  Serial.begin(9600);              // Initiates the serial to do the monitoring 
  Serial.println("Begin motor control");
  Serial.println(); //Print function list for user selection
  Serial.println("Enter number for control option:");
  Serial.println("0. Stop");
  Serial.println("1. Clockwise");
  Serial.println("2. CounterClockwise");
  Serial.println("3. Routine W");
  Serial.println("4. Routine X");
  Serial.println("5. Routine Y");
  Serial.println("6. Routine Z");
  Serial.println();

}

void RoutineW(){
  num = 0;
  delay(1000);
  int pwm = 240;
  //Begin session
  digitalWrite(latchPin, LOW);

  num = 1<<M1CW;
  num = 1<<M2CCW | num;
  num = 1<<M3CW | num;

  shiftOut(dataPin, clockPin, MSBFIRST, num);
  
  digitalWrite(latchPin, HIGH);
  
  analogWrite(PWM_MOTOR_1, pwm); 
  analogWrite(PWM_MOTOR_2, pwm); 
  analogWrite(PWM_MOTOR_3, pwm);
}

void RoutineX(){
  num = 0;
  delay(1000);
  int pwm = 240;
  //Begin session
  digitalWrite(latchPin, LOW);

  num = 1<<M1CCW;
  num = 1<<M2CCW | num;
  num = 1<<M3CCW | num;

  shiftOut(dataPin, clockPin, MSBFIRST, num);
  
  digitalWrite(latchPin, HIGH);
  
  analogWrite(PWM_MOTOR_1, pwm); 
  analogWrite(PWM_MOTOR_2, pwm); 
  analogWrite(PWM_MOTOR_3, pwm);
}

void RoutineY(){
  num = 0;
  delay(1000);
  int pwm = 240;
  //Begin session
  digitalWrite(latchPin, LOW);

  num = 1<<M1CCW;
  num = 1<<M2CW | num;
  num = 1<<M3CCW | num;

  shiftOut(dataPin, clockPin, MSBFIRST, num);
  
  digitalWrite(latchPin, HIGH);
  
  analogWrite(PWM_MOTOR_1, pwm); 
  analogWrite(PWM_MOTOR_2, pwm); 
  analogWrite(PWM_MOTOR_3, pwm);
}

void RoutineZ(){
  num = 0;
  delay(1000);
  int pwm = 240;
  //Begin session
  digitalWrite(latchPin, LOW);

  num = 1<<M1CW;
  num = 1<<M2CW | num;
  num = 1<<M3CW | num;

  shiftOut(dataPin, clockPin, MSBFIRST, num);
  
  digitalWrite(latchPin, HIGH);
  
  analogWrite(PWM_MOTOR_1, pwm); 
  analogWrite(PWM_MOTOR_2, pwm); 
  analogWrite(PWM_MOTOR_3, pwm);
}

void Clockwise()
{
  //Begin session
  digitalWrite(latchPin, LOW);
  
  num = 1<<M1CW;
  num = 1<<M2CW | num;
  num = 1<<M3CW | num;
  
  shiftOut(dataPin, clockPin, MSBFIRST, num);
  int pwm = 40;
      analogWrite(PWM_MOTOR_1, pwm); 

//  for(int pwm=0; pwm<100; pwm+=10){
//    analogWrite(PWM_MOTOR_1, pwm); 
//    analogWrite(PWM_MOTOR_2, pwm); 
//    analogWrite(PWM_MOTOR_3, pwm);
//    delay(100);   
//  }

  //End session
  digitalWrite(latchPin, HIGH);
  delay(1000);
}
void CounterClockwise()
{
  //Begin session
  digitalWrite(latchPin, LOW);
  
  num = 1<<M1CCW;
  num = 1<<M2CCW | num;
  num = 1<<M3CCW | num;
  
  shiftOut(dataPin, clockPin, MSBFIRST, num);

    int pwm = 120;
      analogWrite(PWM_MOTOR_1, pwm); 

//  for(int pwm=0; pwm<250; pwm+=10){
//    analogWrite(PWM_MOTOR_1, pwm); 
//    analogWrite(PWM_MOTOR_2, pwm); 
//    analogWrite(PWM_MOTOR_3, pwm);   
//    delay(100);
//  }

  //End session
  digitalWrite(latchPin, HIGH);
  delay(1000);
}
void Stop()
{
  analogWrite(PWM_MOTOR_1, 0); 
  analogWrite(PWM_MOTOR_2, 0); 
  analogWrite(PWM_MOTOR_3, 0); 
  digitalWrite(latchPin, LOW);
  num = 0;
  shiftOut(dataPin, clockPin, MSBFIRST, num);
  digitalWrite(latchPin, HIGH);
  delay(1000);
}

void loop() 
{
  char user_input;   
  
  while(Serial.available())
  {
    user_input = Serial.read(); //Read user input and trigger appropriate function

//    digitalWrite(EN_PIN_1, HIGH);
//    digitalWrite(EN_PIN_2, HIGH); 

    if(user_input == '1'){
      Serial.println("Clockwise");
        Clockwise();
    }
    else if(user_input == '2'){
      Serial.println("CounterClockwise");
        CounterClockwise();
    }
    else if(user_input == '3'){
      Serial.println("Routine W");
        RoutineW();
    }
    else if(user_input=='4'){
      Serial.println("Routine X");
      RoutineX();
    }
    else if(user_input=='5'){
      Serial.println("Routine Y");
      RoutineY();
    }
    else if(user_input=='6'){
      Serial.println("Routine Z");
      RoutineZ();
    }
    else if(user_input == '0'){
      Serial.println("Stop"); 
      Stop(); 
    }
    else
    {
      Serial.println("Invalid option entered.");
    }
  }
}
