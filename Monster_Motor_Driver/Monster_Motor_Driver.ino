//NODE MCU PIN mapping
#define D0                16
#define D1                5
#define D2                4
#define D3                0
#define D4                2
#define D5                14
#define D6                12
#define D7                13
#define D8                15
#define D9                3
#define D10               1

#define MAX_PWM           200 //1023

#define BRAKE 0
#define CW    1
#define CCW   2
#define CS_THRESHOLD 15   // Definition of safety current (Check: "1.3 Monster Shield Example").

//MOTOR 1
#define MOTOR1_CCW D4
#define MOTOR1_CW D3

//MOTOR 2
#define MOTOR2_CCW D6
#define MOTOR2_CW D5

#define PWM_MOTOR_1 D7
#define PWM_MOTOR_2 D8

//#define CURRENT_SEN_1 2
//#define CURRENT_SEN_2 3

//#define EN_PIN_1 2//A0
//#define EN_PIN_2 3//A1

#define MOTOR_1 0
#define MOTOR_2 1

short usSpeed = 150;  //default motor speed
unsigned short usMotor_Status = BRAKE;
 
void setup()                         
{
  pinMode(MOTOR1_CCW, OUTPUT);
  pinMode(MOTOR1_CW, OUTPUT);

  pinMode(MOTOR2_CCW, OUTPUT);
  pinMode(MOTOR2_CW, OUTPUT);

  pinMode(PWM_MOTOR_1, OUTPUT);
  pinMode(PWM_MOTOR_2, OUTPUT);

  analogWriteFreq(5000);
  analogWriteRange(255);

//  pinMode(CURRENT_SEN_1, OUTPUT);
//  pinMode(CURRENT_SEN_2, OUTPUT);  

//  pinMode(EN_PIN_1, OUTPUT);
//  pinMode(EN_PIN_2, OUTPUT);

  Serial.begin(9600);              // Initiates the serial to do the monitoring 
  Serial.println("Begin motor control");
  Serial.println(); //Print function list for user selection
  Serial.println("Enter number for control option:");
  Serial.println("1. STOP");
  Serial.println("2. FORWARD");
  Serial.println("3. REVERSE");
  Serial.println("4. READ CURRENT");
  Serial.println("+. INCREASE SPEED");
  Serial.println("-. DECREASE SPEED");
  Serial.println();

}
void Pendulum(int count){
  char dir = CCW;
  Serial.println("Pendulum...");
  Stop();
  delay(500);
  for(int i=0;i<count;i++){
    for(int j=0;j<250;j+=10){
      motorGo(MOTOR_1,dir,j);
      motorGo(MOTOR_2,dir,j);
      delay(100);
      //delay(100);
    }
    delay(1000);
    Stop();
    delay(500);
    if(dir==CCW) dir=CW; else dir=CCW;
  }
}

void loop() 
{
  char user_input;   

  
  
  while(Serial.available())
  {
    user_input = Serial.read(); //Read user input and trigger appropriate function
//    digitalWrite(EN_PIN_1, HIGH);
//    digitalWrite(EN_PIN_2, HIGH); 
     
    if (user_input =='1')
    {
       Stop();
    }
    else if(user_input =='2')
    {
      Forward();
    }
    else if(user_input =='3')
    {
      Reverse();
    }
    else if(user_input =='+')
    {
      IncreaseSpeed();
    }
    else if(user_input =='-')
    {
      DecreaseSpeed();
    }
    else if(user_input =='4')
    {
      Pendulum(6);
    }
    else
    {
      Serial.println("Invalid option entered.");
    }
      
  }
}

void Stop()
{
  Serial.println("Stop");
  usMotor_Status = BRAKE;
  motorGo(MOTOR_1, usMotor_Status, 0);
  motorGo(MOTOR_2, usMotor_Status, 0);
}

void Forward()
{
  Serial.println("Forward");
  usMotor_Status = CW;
  motorGo(MOTOR_1, usMotor_Status, usSpeed);
  motorGo(MOTOR_2, usMotor_Status, usSpeed);
}

void Reverse()
{
  Serial.println("Reverse");
  usMotor_Status = CCW;
  motorGo(MOTOR_1, usMotor_Status, usSpeed);
  motorGo(MOTOR_2, usMotor_Status, usSpeed);
}

void IncreaseSpeed()
{
  usSpeed = usSpeed + 1;
  if(usSpeed > MAX_PWM)
  {
    usSpeed = MAX_PWM;  
  }
  
  Serial.print("Speed +: ");
  Serial.println(usSpeed);

  motorGo(MOTOR_1, usMotor_Status, usSpeed);
  motorGo(MOTOR_2, usMotor_Status, usSpeed);  
}

void DecreaseSpeed()
{
  usSpeed = usSpeed - 1;
  if(usSpeed < 0)
  {
    usSpeed = 0;  
  }
  
  Serial.print("Speed -: ");
  Serial.println(usSpeed);

  motorGo(MOTOR_1, usMotor_Status, usSpeed);
  motorGo(MOTOR_2, usMotor_Status, usSpeed);  
}

void motorGo(uint8_t motor, uint8_t direct, uint8_t pwm)         //Function that controls the variables: motor(0 ou 1), direction (cw ou ccw) e pwm (entra 0 e MAX_PWM);
{
  if(motor == MOTOR_1)
  {
    if(direct == CW)
    {
      digitalWrite(MOTOR1_CCW, LOW); 
      digitalWrite(MOTOR1_CW, HIGH);
    }
    else if(direct == CCW)
    {
      digitalWrite(MOTOR1_CCW, HIGH);
      digitalWrite(MOTOR1_CW, LOW);      
    }
    else
    {
      digitalWrite(MOTOR1_CCW, LOW);
      digitalWrite(MOTOR1_CW, LOW);            
    }
    
    analogWrite(PWM_MOTOR_1, pwm); 
  }
  else if(motor == MOTOR_2)
  {
    if(direct == CW)
    {
      digitalWrite(MOTOR2_CCW, LOW);
      digitalWrite(MOTOR2_CW, HIGH);
    }
    else if(direct == CCW)
    {
      digitalWrite(MOTOR2_CCW, HIGH);
      digitalWrite(MOTOR2_CW, LOW);      
    }
    else
    {
      digitalWrite(MOTOR2_CCW, LOW);
      digitalWrite(MOTOR2_CW, LOW);            
    }
    
    analogWrite(PWM_MOTOR_2, pwm);
  }
}
