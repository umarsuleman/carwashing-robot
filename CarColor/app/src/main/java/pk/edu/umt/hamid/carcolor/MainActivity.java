package pk.edu.umt.hamid.carcolor;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.os.Environment;
import android.support.annotation.ColorInt;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import java.io.File;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ImageView image1, image2, image3, image4;
    View carColor;
    File mFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AskPermissions();

        image1 = findViewById(R.id.image1);
        image2 = findViewById(R.id.image2);
        image3 = findViewById(R.id.image3);
        image4 = findViewById(R.id.image4);
        carColor = findViewById(R.id.carColor);
        setImages();
    }

    public void setImages(){
        //Change Directory
        mFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"side6.mp4");

        final MediaMetadataRetriever retriever = new MediaMetadataRetriever();

        try {
            retriever.setDataSource(mFile.getPath());

            image1.setImageBitmap(retriever.getFrameAtTime(1000000,MediaMetadataRetriever.OPTION_CLOSEST));
            image2.setImageBitmap(retriever.getFrameAtTime(2000000,MediaMetadataRetriever.OPTION_CLOSEST));
            image3.setImageBitmap(retriever.getFrameAtTime(8000000,MediaMetadataRetriever.OPTION_CLOSEST));
            //image4.setImageBitmap(retriever.getFrameAtTime(2000000,MediaMetadataRetriever.OPTION_CLOSEST));
            int carcolor = GetCarColor(retriever.getFrameAtTime(1000000,MediaMetadataRetriever.OPTION_CLOSEST ));
            carColor.setBackgroundColor(carcolor);
            Bitmap highlightedImage = HighlightCarColor(retriever.getFrameAtTime(1000000,MediaMetadataRetriever.OPTION_CLOSEST),carcolor);
            image4.setImageBitmap(highlightedImage);
        } catch (IllegalArgumentException ex) {
            ex.printStackTrace();
        } catch (RuntimeException ex) {
            ex.printStackTrace();
        } finally {
            try {
                retriever.release();
            } catch (RuntimeException ex) {
            }
        }
    }

    public int GetCarColor(Bitmap bitmap){
        int height = bitmap.getHeight();
        int width = bitmap.getWidth();
        ArrayList<Integer> AllColors = new ArrayList<>();
        ArrayList<Integer> ColorsCount = new ArrayList<>();
        int index = 0;
        int color ;
        //int Alpha, Red, Green, Blue;
        for(int x=0; x<width; x++){
            for(int y=0; y<height; y++){
                if(y>1600 || y<50)
                    continue;
                color = bitmap.getPixel(x,y);
                color = ColorCorrect(color);
                if (!AllColors.contains(color)){
                    AllColors.add(color);
                    ColorsCount.add(1);
                }
                else{
                    index = AllColors.indexOf(color);
                    ColorsCount.set(index,ColorsCount.get(index)+ 1);
                }
            }
        }
        int max = 0;
        int size = ColorsCount.size();
        for(int i = 0; i<size; i++){
            if(max<ColorsCount.get(i)){
                index = i;
                max = ColorsCount.get(i);
            }
        }
//        //second max
//        for(int i = 0; i<size; i++){
//            if(max2<ColorsCount.get(i)){
//                if(ColorsCount.get(i) != max){
//                    max2 = ColorsCount.get(i);
//                    index2 = i;
//                }
//            }
//        }
        color = AllColors.get(index);
//        Alpha = (color >> 24) & 0xff; // or color >>> 24
//        Red = (color >> 16) & 0xff;
//        Green = (color >> 8) & 0xff;
//        Blue = (color) & 0xff;
//
//        color =  (Alpha & 0xff) << 24 | (Red & 0xff) << 16 | (Green& 0xff) << 8 | (Blue & 0xff);
        return color;
    }

    public Bitmap HighlightCarColor(Bitmap image, int carcolor){
        int height = image.getHeight();
        int width = image.getWidth();
        //Defining the highlight color red
        int highlight =  (255 & 0xff) << 24 | (255 & 0xff) << 16 | (0 & 0xff) << 8 | (0 & 0xff);
        int color;
        for(int x=0; x<width; x++){
            for(int y=0; y<height; y++){
                color = image.getPixel(x,y);
                color = ColorCorrect(color);
                if(checkColor(carcolor,color)){
                    image.setPixel(x,y,highlight);
                }
            }
        }
        return image;
    }

    public boolean checkColor(int carcolor, int pixelcolor){
        int alpha, red, green, blue;
        int newred, newgreen, newblue;
        int limit = 32;
        //Getting the rgb colors
        alpha = (carcolor >> 24) & 0xff; // or color >>> 24
        red = (carcolor >> 16) & 0xff;
        green = (carcolor >> 8) & 0xff;
        blue = (carcolor) & 0xff;
        if(red+limit <= 255)
            newred = red+limit;
        else
            newred = 255;
        if(green+limit <= 255)
            newgreen = green+limit;
        else
            newgreen = 255;
        if(blue+limit <= 255)
            newblue = blue+limit;
        else
            newblue = 255;

        int upperlimit = (alpha & 0xff) << 24 | (newred & 0xff) << 16 | (newgreen & 0xff) << 8 | (newblue & 0xff);

        if(red-limit >= 0)
            newred = red-limit;
        else
            newred = 0;
        if(green-limit >= 0)
            newgreen = green-limit;
        else
            newgreen = 0;
        if(blue-limit >= 255)
            newblue = blue-limit;
        else
            newblue = 0;

        int lowerlimit = (alpha & 0xff) << 24 | (newred & 0xff) << 16 | (newgreen & 0xff) << 8 | (newblue & 0xff);

        if((pixelcolor>=lowerlimit) && (pixelcolor<=upperlimit)){
            return true;
        }
        return false;
    }

    public int ColorCorrect(int color){
        int difference = 32;
        int alpha, red, green, blue;

        //Getting the rgb colors
        alpha = (color >> 24) & 0xff; // or color >>> 24
        red = (color >> 16) & 0xff;
        green = (color >> 8) & 0xff;
        blue = (color) & 0xff;

        //setting the new rgb colors
        red /= difference;
        green /= difference;
        blue /= difference;
        red *= difference;
        green *= difference;
        blue *= difference;
        red += (difference/2);
        green += (difference/2);
        blue += (difference/2);

        //saving the new rgb colors
        color =  (alpha & 0xff) << 24 | (red & 0xff) << 16 | (green& 0xff) << 8 | (blue & 0xff);
        return color;
    }

    private void AskPermissions(){
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    1);
        }
    }

}
